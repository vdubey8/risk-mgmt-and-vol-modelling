/*
	Name: Varun Dubey
	GTID: 903225433
*/

libname storage "Q:\Scratch\vd8\7";
libname CRSPDATA "Q:\Data-ReadOnly\CRSP";

/* MACRO - SORT DATA */
%macro sort_table(param1, param2);
	proc sort data = &param1;
		by &param2;
	run;
%mend;

	/* EXTRACT IBM AND GE DATA FROM CRSP DATASET */
	proc sql;
		create table IBM_GE_data as 
		select a.permno, a.date, a.ret
		from CRSPDATA.dsf as a
		where a.permno = 12490 or a.permno = 12060;
	quit;

	proc sql;
		create table part1 as
		select a.*
		from IBM_GE_data as a 
		where a.date >= '01Jan2001'd and a.date <= '31Dec2006'd;

		create table part2 as
		select a.*
		from IBM_GE_data as a 
		where a.date >= '01Jan2001'd and a.date <= '31Dec2009'd;
	quit;

	%sort_table(part1,date permno);
	%sort_table(part2,date permno);

	proc transpose data=part1 out=trans_part1;
		by date;
		id permno;
		var ret;
	run;

	proc transpose data=part2 out=trans_part2;
		by date;
		id permno;
		var ret;
	run;
	
	/* CREATE PORTFOLIOS */
	data port_part1;
		set trans_part1;

		ret = 0.5 * (_12060 + _12490);
		portval = 1000000 * (1+ret);
		keep date ret portval;
	run;

	data port_part2;
		set trans_part2;

		ret = 0.5 * (_12060 + _12490);
		portval = 1000000 * (1+ret);
		keep date ret portval;
	run;


ods graphics on;
ods pdf file = "P:\MGMT of Fin\A7\Assignment_7.pdf";

/* --------------------- 5% VAR AND DOLALR VAR FOR EACH SAMPLE PERIOD ------------------ */
	proc univariate data = port_part1 noprint;
		var ret portval;
		histogram ret;
		output out = part1_var_values pctlpts=5 pctlpre=VaR_ dollar_VaR_;
	run;

	proc univariate data = port_part2 noprint;
		var ret portval;
		histogram ret;
		output out = part2_var_values pctlpts=5 pctlpre=VaR_ dollar_VaR_;
	run;

	title 'VaR DURING 2001-2006 PERIOD';

	proc print data = part1_var_values;
	run;
	quit;

	title 'VaR DURING 2001-2009 PERIOD';

	proc print data = part2_var_values;
	run;
	quit;


/* ---------------------------------EXPECTED SHORTFALL-------------------------------- */
	proc rank data = port_part1 out=part1_rank groups = 100;
		var ret;
		ranks group;
	run;

	proc rank data = port_part2 out=part2_rank groups = 100;
		var ret;
		ranks group;
	run;

	proc sql;
		create table part1_tail as
		select a.*, mean(a.ret) as ES1
		from part1_rank as a
		where a.group<=4;

		create table part2_tail as
		select a.*, mean(a.ret) as ES2
		from part2_rank as a
		where a.group<=4;
	quit;
	
	proc sort data = part1_tail nodupkey;
		by ES1;
	run;
	proc sort data = part2_tail nodupkey;
		by ES2;
	run;

	data ES1;
		set part1_tail;
		keep ES1;
	run;
	
	data ES2;
		set part2_tail;
		keep ES2;
	run;
	
	title 'EXPECTED SHORTFALL DURING 2001-2006 PERIOD';
	proc print data = ES1;
	run;
	quit;

	title 'EXPECTED SHORTFALL DURING 2001-2009 PERIOD';
	proc print data = ES2;
	run;
	quit;

/* -------------------------------------------JPMORGAN RISKMETRICS ------------------------------------*/
	proc sql;
		create table storage.IBM_part1 as
		select std(a.ret) as var_0, mean(a.ret) as ret_0
		from IBM_GE_data as a
		where a.permno = 12490 and (a.date>='01Jan1996'd and a.date <= '31Dec2000'd);

		create table storage.GE_part1 as
		select std(a.ret) as var_0, mean(a.ret) as ret_0
		from IBM_GE_data as a
		where a.permno = 12060 and (a.date>='01Jan1996'd and a.date <= '31Dec2000'd);
	quit;

	proc sql;
		create table storage.IBM_part2 as
		select a.permno, a.date, a.ret
		from IBM_GE_data as a 
		where a.permno = 12490 and (a.date>='01Jan2001'd and a.date <= '31Dec2009'd);

		create table storage.GE_part2 as
		select a.permno, a.date, a.ret
		from IBM_GE_data as a
		where a.permno = 12060 and (a.date>='01Jan2001'd and a.date <= '31Dec2009'd);
	quit;

	data storage.IBM_part1; 
		set storage.IBM_part1; 
		ret = ret_0; 
		drop ret_0; 
	run;

	data storage.GE_part1; 
		set storage.GE_part1; 
		ret = ret_0; 
		drop ret_0; 
	run;

	data storage.IBM_combined;
		set storage.IBM_part1 storage.IBM_part2;

		ret_lag = lag(ret);
		firmvar_lag = .;
		retain firmvar;
		firmvar_lag = firmvar;
		if var_0 ne . then firmvar = (var_0**2);
		else firmvar = 0.94*firmvar_lag + (1-0.94)*(ret_lag**2);
		drop var_0;
	run;

	data storage.GE_combined;
		set storage.GE_part1 storage.GE_part2;

		ret_lag = lag(ret);
		firmvar_lag = .;
		retain firmvar;
		firmvar_lag = firmvar;
		if var_0 ne . then firmvar = (var_0**2);
		else firmvar = 0.94*firmvar_lag + (1-0.94)*(ret_lag**2);
		drop var_0;
	run;


	title "IBM VARIANCE USING RiskMetrics MODEL";
	proc gplot data = storage.IBM_combined;	
		plot firmvar*date;
		symbol1 value=plus color=red;
	run;

	title "GE VARIANCE USING RiskMetrics MODEL";
	proc gplot data = storage.GE_combined;
		plot firmvar*date;
		symbol1 value=plus color=red;
	run;
	quit;
	
/* --------------------------------------------- GARCH MODEL ----------------------------------------------- */
	data garch_data;
		set IBM_GE_data;

		if date>='01Jan1996'd and date <= '31Dec2000'd;
		if permno = 12490 or permno = 12060;
	run;

	data storage.IBM_garch;
		set garch_data;
		if permno = 12490;
	run;

	data storage.GE_garch;
		set garch_data;
		if permno = 12060;
	run;

	title "GARCH AUTOREG RESULTS: IBM";
	proc autoreg data = storage.IBM_garch outest = storage.IBM_coeffs;
		model ret = / garch=(p=1,q=1);
		output out = storage.IBM_autoreg_out cev = vhat;
	run;

	title "GARCH AUTOREG RESULTS: GE";
	proc autoreg data = storage.GE_garch outest = storage.GE_coeffs;
		model ret = / garch=(p=1,q=1);
		output out = storage.GE_autoreg_out cev = vhat;
	run;

/* CREATING GARCH DATA FOR IBM */
	data storage.IBM_coeffs;
		set storage.IBM_coeffs;

		PERMNO=12490;
		keep PERMNO _AH_0 _AH_1 _GH_1;
	run;

	data storage.IBM_after_garch;
		set storage.IBM_autoreg_out storage.IBM_part2;
	run;

	data storage.IBM_after_garch;
		merge storage.IBM_after_garch(in=a) storage.IBM_coeffs(in=b);
		by permno;
		if a;

		ret_lag = lag(ret);
		firmvar_lag = .;

		retain firmvar;
		firmvar_lag = firmvar;
		if vhat ne . then firmvar = (vhat**2);
		/* variance = w + alpha*return^2 + beta*variance */
		else firmvar = _AH_0+_AH_1*firmvar_lag + _GH_1*(ret_lag**2);

		if date < '31Dec2000'd then delete;
		drop vhat;
	run;

/* CREATING GARCH DATA FOR GE */
	data storage.GE_coeffs;
		set storage.GE_coeffs;
		PERMNO=12060;
		keep PERMNO _AH_0 _AH_1 _GH_1;
	run;

	data storage.GE_after_garch;
		set storage.GE_autoreg_out storage.GE_part2;
	run;

	data storage.GE_after_garch;
		merge storage.GE_after_garch(in=a) storage.GE_coeffs(in=b);
		by PERMNO;
		if a;

		ret_lag = lag(ret);
		firmvar_lag = .;

		retain firmvar;
		firmvar_lag = firmvar;
		if vhat ne . then firmvar = (vhat**2);
		/* variance = w + alpha*return^2 + beta*variance */
		else firmvar = _AH_0+_AH_1*firmvar_lag + _GH_1*(ret_lag**2);

		if date < '31Dec2000'd then delete;
		drop vhat;
	run;


/* ----------------------------------- FINAL -------------------------------------------- */

	title "IBM VARIANCE USING GARCH MODEL";
	proc gplot data = storage.IBM_after_garch;
		plot firmvar*date;
		symbol1 value=plus color=red;
	run;
	
	title "GE VARIANCE USING GARCH MODEL";
	proc gplot data = storage.GE_after_garch;
		plot firmvar*date;
		symbol1 value=plus color=red;
	run;
	
ods pdf close;
quit;
